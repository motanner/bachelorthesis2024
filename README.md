# BachelorThesis2024

In this repository are (most) of the files related to my bachelor thesis project from 2024 for future reference (limited to 1GB). For the analysis of the experimental data, the spectrum of the raw date was calculated (numpy mean over one axis) on the server and exported in a npy file for offline work. In a second step, the raw data was further compressed into feature arrays. Both spectrum array and feature arrays have to be generated a second time, as the exceed the 1GB data limit. 

For any questions on the plots, images and scripts, feel free to contact me under motanner@ethz.ch. 

- AnalysisCode consists of all the python scripts that were used to generate the plots and figures in the analysis of the experimental data as well as some individual plots and image scans. 

- CombinedImages consists of all the (often large) plots that combining data from different datasets.

- Literature includes my bachelor thesis text as well as several literature papers that are relevant.

- Presentations includes all three powerpoint slides (keynote files apple).

- SampleImages includes individual plots of interest that look at one specific data point.

- SimulationCode includes the python script with functions used in the thesis (ODE approach) as well as partial other approaches with an ODE approach including Monte Carlo randomness and one where the entire spectrum is discretized instead of only considering 2 photon energies. 


PS. I found a paper from 2022 (literature section) that solves this type of ODE (vector valued quadratic polynomial right hand side) analytically and discusses the behavior of solutions. 

