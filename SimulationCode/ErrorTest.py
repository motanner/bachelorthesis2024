import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import Simplesimsalabim as Sim

def RUN(X, T, t, Phi, DPhi=False, Rtol = 0.001):
    G,H,I,J,K = [X[0]], [X[1]], [X[2]], [X[3]], [0]
    if DPhi == False:
        INT = sp.integrate.RK45(fun=Phi, t0=0, y0=X, t_bound=T, rtol=Rtol)
    else:
        INT = sp.integrate.Radau(fun=Phi,t0=0,y0=X,t_bound=T, jac=DPhi, rtol=Rtol)
    n = 0 
    while INT.t < T:
        n+=1
        INT.step()
        G.append(INT.y[0])
        H.append(INT.y[1])
        I.append(INT.y[2])
        J.append(INT.y[3])
        K.append(INT.t)
    return n,G,H,I,J,K



def CALC(E,B,P,W,L,Gr,Br,Kr,Sr,Wr, TOL, Radau):
    W = W*Wr
    G,H,I,J = 0,0,0,0
    C = 299792458                   #   Speed of Light      [m/s]
    D = 2.52e-15                    #   Lifetime Ion        [s]
    S = B*3e-8                      #   Pulse Durations     [m]
    T = C/L                         #   Final Time          [s] 
    V = np.pi/4*S*W*W               #   Pulse Volume        [m3]
    M = 6.242e12*B/E                #   Number of Photons   [#]
    N = 100*P*V/1.380649e-23/297    #   Number of Neon      [#]
    
    Alpha   = C/S                                                   #   Entry/Exit Raty
    Beta    = 1/D*Br                                                   #   Decay Rate
    Gamma   = 0.018*W*W/4/L/L*Gr                                    #   Spon. Em. Rate
    Sigma   = 8e-23*(5*(E-870.3)/(1 + 5*np.abs(E-870.3))+1)/2*C/V*Sr
    Kappa   = 1e-21*C/V*Kr                                             #   Stim. Em. Cross Section

    PHI     =   Sim.Make_Phi(N, Alpha,Beta,Gamma,Sigma,Kappa)
    DPHI    =   Sim.Make_DPhi(Alpha,Beta,Gamma,Sigma,Kappa)
    print(TOL)
    if Radau: 
        return RUN( np.asarray([0,0,M,0]), L/C, S/C, PHI, DPHI, Rtol = TOL)
    else:
        return RUN( np.asarray([0,0,M,0]), L/C, S/C, PHI, DPhi=False, Rtol = TOL)

def ELoop(Erange, B, P, W, L, Gr,Br,Kr,Sr,Wr):
    Ilist,Jlist = [],[]
    for e in Erange:
        #print(e,B,P)
        G,H,I,J,K = CALC( e, B, P, W, L, Gr,Br,Kr,Sr,Wr)
        Ilist.append(I[-1]); Jlist.append(J[-1])
    return Ilist, Jlist

def PLoop(E, B, Prange, W, L, Gr,Br,Kr,Sr,Wr):
    Ilist,Jlist = [],[]
    for p in Prange:
        #print(E,B,p)
        G,H,I,J,K = CALC( E, B, p, W, L, Gr,Br,Kr,Sr,Wr)
        Ilist.append(I[-1]); Jlist.append(J[-1])
    return Ilist, Jlist

def plot(ax, Range, ABS, LAS, RangeLabel, Titel, ABScolor, LAScolor):
    ax.set_title(Titel)
    ax.set_xlabel(RangeLabel)
    ax.set_ylabel("Photons")
    ax.plot(Range, ABS, c=ABScolor)
    ax.plot(Range, LAS, c=LAScolor) 



NlistI, ElistI = [],[]
nokI, GokI,HokI,IokI,JokI,KokI = CALC( 880, 870, 500, 0.0000024, 0.01, 1,1,1,1,1, 2**-60, True)
IOKI = IokI[-1]; JOKI = JokI[-1]
for tol in range(1,40):
    Tol = 2**-tol
    n, G,H,I,J,K = CALC( 880, 870, 500, 0.0000024, 0.01, 1,1,1,1,1, Tol, True)
    NlistI.append(n);
    ElistI.append( (I[-1]-IOKI)**2 + (J[-1]-JOKI)**2 )

NlistE, ElistE = [],[]
nokE, GokE,HokE,IokE,JokE,KokE = CALC( 880, 870, 500, 0.0000024, 0.01, 1,1,1,1,1, 2**-60, False)
IOKE = IokE[-1]; JOKE = JokE[-1]
for tol in range(1,40):
    Tol = 2**-tol
    n, G,H,I,J,K = CALC( 880, 870, 500, 0.0000024, 0.01, 1,1,1,1,1, Tol, False)
    NlistE.append(n);
    ElistE.append( (I[-1]-IOKE)**2 + (J[-1]-JOKE)**2 )


fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(8,4))
axs[0].loglog( 2**-np.arange(1,40,dtype=np.float), NlistI, marker = "+", linestyle = "None", color="black")
axs[1].loglog( 2**-np.arange(1,40,dtype=np.float), NlistE, marker = "+", linestyle = "None", color="black")
axs[0].set_title("A")
axs[0].set_xlabel("rtol")
axs[0].set_ylabel("Number of steps")
axs[1].set_title("B")
axs[1].set_xlabel("rtol")
axs[1].set_ylabel("Number of steps")
plt.tight_layout()
plt.show()

