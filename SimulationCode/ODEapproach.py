import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import Simplesimsalabim as Sim

def Make_Phi(N, Alpha,Beta,Gamma,Kappa,Sigma,Mu,Nu):
    def Phi(t,Y):
        G,H,I,J = Y[0], Y[1], Y[2], Y[3]
        return np.asarray([
            -Alpha*G + Alpha*N - Sigma*I*G,
            -Alpha*H - Beta*H  + Sigma*I*G - Kappa*J*H,
            -Mu*I*G,
            Nu*J*H + Gamma*H
            ])
    return Phi

def Make_DPhi(Alpha,Beta,Gamma,Kappa,Sigma,Mu,Nu):
    def DPhi(t,Y):
        G,H,I,J =  Y[0], Y[1], Y[2], Y[3]
        return np.asarray([
            [-Alpha-Sigma*I, 0, -Sigma*G, 0],
            [Sigma*I, -Alpha-Beta-Kappa*J, Sigma*G, -Kappa*H],
            [-Mu*I, 0, -Mu*G, 0],
            [0, Nu*J+Gamma, 0, Nu*H]
            ])
    return DPhi


def RUN(X, T, Phi, DPhi=False):
    G,H,I,J,K = [X[0]], [X[1]], [X[2]], [X[3]], [0]
    if DPhi == False:
        INT = sp.integrate.RK45(fun=Phi, t0=0, y0=X, t_bound=T, max_step=t)
    else:
        INT = sp.integrate.Radau(fun=Phi,t0=0,y0=X,t_bound=T, jac=DPhi)
    while INT.t < T:
        INT.step()
        G.append(INT.y[0])
        H.append(INT.y[1])
        I.append(INT.y[2])
        J.append(INT.y[3])
        K.append(INT.t)
    return G,H,I,J,K

def CALC(E,B,P,Alpha,Beta,Gamma,Kappa,Sigma,Mu,Nu):

    PHI     =   Make_Phi(P, Alpha,Beta,Gamma,Sigma,Kappa,Mu,Nu)
    DPHI    =   Make_DPhi(Alpha,Beta,Gamma,Sigma,Kappa,Mu,Nu)
    
    return RUN( np.asarray([0,0,B,0]), 0.01/299792458, PHI, DPHI)

def ELoop(Erange, B, P, Beta,Gamma,Kappa,Sigma,Mu,Nu):
    Ilist,Jlist = [],[]
    for e in Erange:
        G,H,I,J,K = CALC( e, B, P, Beta,Gamma,Kappa,Sigma,Mu,Nu)
        Ilist.append(I[-1]); Jlist.append(J[-1])
    return Ilist, Jlist

def PLoop(E, B, Prange, Alpha, Beta,Gamma,Kappa,Sigma,Mu,Nu):
    Ilist,Jlist = [],[]
    for p in Prange:
        G,H,I,J,K = CALC( E, B, p, Alpha, Beta,Gamma,Kappa,Sigma,Mu,Nu)
        Ilist.append(I[-1]); Jlist.append(J[-1])
    return Ilist, Jlist

def plot(ax, Range, ABS, LAS, RangeLabel, Titel, ABScolor, LAScolor):
    ax.set_title(Titel)
    ax.set_xlabel(RangeLabel)
    ax.set_ylabel("Photons")
    ax.plot(Range, ABS, c=ABScolor)
    ax.plot(Range, LAS, c=LAScolor)


Parameters = [1e-15,1e-10,1e-21,1e-22,10,1]

Erange = np.arange(860, 880, 2)
Prange = np.arange(0, 1000, 100)

fig, axs = plt.subplots(nrows=2,ncols=2,figsize=(8,8))

ABS, LAS = ELoop(Erange, 870, 500, *Parameters)
plot(axs[0,0], Erange, ABS, LAS, "SASE Photon Energy [eV]", "A", "blue", "green")

ABS, LAS = ELoop(Erange, 300, 500, *Parameters)
plot(axs[0,1], Erange, ABS, LAS, "SASE Photon Energy [eV]", "B", "blue", "green")

ABS, LAS = PLoop(880, 895, Prange, *Parameters)
plot(axs[1,0], Prange, ABS, LAS, "Neon Gas Pressure [mBar]", "C", "blue", "green")

ABS, LAS = PLoop(880, 140, Prange, *Parameters)
plot(axs[1,1], Prange, ABS, LAS, "Neon Gas Pressure [mBar]", "D", "blue", "green")


plt.show()

        




"""


POINTS = [
    [860, 870, 500, 0.0000024,  150, 0],
    [865, 870, 500, 0.0000024,  175, 0],
    [870, 870, 500, 0.0000024,  125, 75],
    [873, 870, 500, 0.0000024,  90, 90],
    [880, 870, 500, 0.0000024,  90, 90],

    [860, 300, 500, 0.0000024,  175, 0],
    [865, 300, 500, 0.0000024,  175, 0],
    [870, 300, 500, 0.0000024,  75, 15],
    [873, 300, 500, 0.0000024,  0, 25],
    [880, 300, 500, 0.0000024,  0, 25],

    [880, 900, 100, 0.0000024,  350, 0],
    [880, 900, 200, 0.0000024,  250, 50],
    [880, 900, 400, 0.0000024,  125, 125],
    [880, 900, 600, 0.0000024,  0, 150],
    [880, 900, 500, 0.0000024,  0, 150],

    [880, 140, 500, 0.0000024,  8, 8],
    [880, 140, 700, 0.0000024,  2, 8]

    ]



def OPT(X):
    Scale, Beta, Gamma, Kappa, Sigma, Mu, Nu = X[0], X[1], X[2], X[3], X[4], X[5], X[6]
    Error = 0
    for p in POINTS:
        G,H,I,J,K = CALC( p[0], p[1], p[2], Beta, Gamma, Kappa, Sigma, Mu, Nu)
        Error += (I[-1]*Scale - p[-2])**2 + (J[-1]*Scale - p[-1])**2
    return Error


#Result = sp.optimize.minimize(OPT, np.asarray([1e-10,1e-15,1e-10,1e-21,1e-22,1,1]), method='Nelder-Mead')


"""




"""
def CALC(E,B,P,W,L,Br,Gr,Sr,Kr):
    G,H,I,J = 0,0,0,0
    C = 299792458                   #   Speed of Light      [m/s]
    D = 2.52e-15                    #   Lifetime Ion        [s]
    S = B*3e-8                      #   Pulse Durations     [m]
    T = C/L                         #   Final Time          [s] 
    V = np.pi/4*S*W*W               #   Pulse Volume        [m3]
    M = 6.242e12*B/E                #   Number of Photons   [#]
    N = 100*P*V/1.380649e-23/297    #   Number of Neon      [#]
    
    Alpha   = C/S                                                   #   Entry/Exit Raty
    Beta    = 1/D*Br                                                   #   Decay Rate
    Gamma   = 0.018*W*W/4/L/L*Gr                                    #   Spon. Em. Rate
    Sigma   = 8e-23*(5*(E-870.3)/(1 + 5*np.abs(E-870.3))+1)/2*C/V*Sr   #   Ionization Cross Section
    Kappa   = 1e-21*C/V*Kr                                             #   Stim. Em. Cross Section

    PHI     =   Sim.Make_Phi(N, Alpha,Beta,Gamma,Sigma,Kappa)
    DPHI    =   Sim.Make_DPhi(Alpha,Beta,Gamma,Sigma,Kappa)
    
    return Sim.RUN( np.asarray([0,0,M,0]), L/C, S/C, PHI, DPHI)

POINTS = [
    [860, 870, 500, 0.0000024,  150, 0],
    [865, 870, 500, 0.0000024,  175, 0],
    [870, 870, 500, 0.0000024,  125, 75],
    [873, 870, 500, 0.0000024,  90, 90],
    [880, 870, 500, 0.0000024,  90, 90],

    [860, 300, 500, 0.0000024,  175, 0],
    [865, 300, 500, 0.0000024,  175, 0],
    [870, 300, 500, 0.0000024,  75, 15],
    [873, 300, 500, 0.0000024,  0, 25],
    [880, 300, 500, 0.0000024,  0, 25],

    [880, 900, 100, 0.0000024,  350, 0],
    [880, 900, 200, 0.0000024,  250, 50],
    [880, 900, 400, 0.0000024,  125, 125],
    [880, 900, 600, 0.0000024,  0, 150],
    [880, 900, 500, 0.0000024,  0, 150],

    [880, 140, 500, 0.0000024,  8, 8],
    [880, 140, 700, 0.0000024,  2, 8]

    ]

def OPT(X):
    Su, Sb, Sg, Ss, Sk = X[0], X[1], X[2], X[3], X[4]
    Error = 0
    for p in POINTS:
        G,H,I,J,K = CALC( p[0], p[1], p[2], 0.0000024, 0.01, Sb, Sg, Ss, Sk)
        Error += (I[-1]*Su - p[-2])**2 + (J[-1]*Su - p[-1])**2
    return Error


result = sp.optimize.minimize(OPT, np.asarray([1e-10, 1,1,1,1]) )


print(result.message)
print(result.success)
print(result.x)
"""





    
    
    
