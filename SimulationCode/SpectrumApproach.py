###############################################################################
###     BACHELOR THESIS                                                     ###
###     IVP SIMULATION OF XRAY PUMPED NEON LASER                            ###
###     MORITZ TANNER 2024                                                  ###
###     ETHZ & PSI                                                          ###
###############################################################################

import numpy as np
import scipy as sp

def Get_Sase(X, Range, Emin):
    return np.amax(X[2:][Range>Emin])

def Get_Laser(X, Range,Emax):
    return np.amax(X[2:][Range<Emax])

def Make_Kappa(Range, Xpeak, Ypeak, Width):
    kappa =  1/(np.pi*Width*(1+((Range-Xpeak)/Width)**2))
    kappa = kappa / np.amax(kappa) * Ypeak
    return kappa

def Make_Kappa2(Range, Xpeak, Ypeak, Width):
    kappa =  1/np.sqrt(np.pi*Width**2)*np.exp(-(Range-Xpeak)**2/Width**2)
    kappa = kappa / np.amax(kappa) * Ypeak
    return kappa

def Make_Gamma(Range, Xpeak, Ypeak, Width):
    return Ypeak/(np.pi*Width*(1+((Range-Xpeak)/Width)**2))

def Make_Sigma(Range, Xpeak, Ypeak, Speed):
    return Ypeak*(Speed*(Range-Xpeak) / (1 + Speed*np.abs(Range-Xpeak))+1)/2

def Make_SASE(Range, Mean, Var, Intensity):
    sase = np.exp( -(Range-Mean)**2 / Var**2)
    sase = sase/ sase.sum()
    SASE = np.random.binomial(Intensity, sase)
    return SASE

def Make_Theta(Range, Emean, Evar, I):
    return I/Evar/np.sqrt(2*np.pi)*np.exp( -(Range-Emean)**2 / Evar**2)

def Make_SASE_Normal(Range,Mean,Var,Intensity):
    return SASE

def Make_Phi(N,Alpha,Beta,Gamma,Sigma,Kappa,V):
    C = 299792458
    def Phi(t,Y):
        X,G,H,I = np.zeros(Y.shape[0]), Y[0], Y[1], Y[2:]
        X[0]    = -Alpha*G + Alpha*N - (Sigma*I*G*C/V).sum()
        X[1]    = -Alpha*H - Beta*H - (Kappa*I*H*C/V).sum() + (Sigma*I*G*C/V).sum()
        X[2:]   = Kappa*I*H*C/V - Sigma*I*G*C/V + Gamma*Beta*H
        return X
    return Phi

def Make_DPhi(N,Alpha,Beta,Gamma,Sigma,Kappa,V):
    C = 299792458
    def DPhi(t,Y):
        X,G,H,I = np.zeros((Y.shape[0],Y.shape[0])),Y[0], Y[1], Y[2:]
        X[0,0] = - Alpha - (Sigma*I*C/V).sum()
        X[1,0] = (Sigma*I*C/V).sum()
        X[1,1] = - Alpha - Beta - (Kappa*I*C/V).sum()
        X[0,2:] = -Sigma*G/V
        X[1,2:] = Sigma*G/V - Kappa*H/V
        X[2:,0] = -Sigma*I/V
        X[2:,1] = Kappa*I/V + Gamma*Beta
        X[2:,2:] = np.diag(Kappa*H/V - Sigma*G/V)
        return X
    return DPhi

def ExplEuler(X0,Phi,dt,TS):
    X = np.zeros((TS,X0.shape[0])); X[0,:] = X0
    for t in range(1,TS): X[t,:] = X[t-1,:] + dt*Phi(t,X[t-1,:])
    return X

def ImplEuler(): pass

def RK45(Range,Y0, Phi, Tf, tmax):
    S = []; S.append(Get_Sase(Y0, Range, 855))
    L = []; L.append(Get_Laser(Y0, Range, 855))
    T = []; T.append(0)
    INT = sp.integrate.RK45(fun=Phi,t0=0,y0=Y0,t_bound=Tf, max_step=0.1*tmax) #rtol=1e-14 max_step=tmax max_step=8.906161341790659e-18
    n = 0
    while INT.t<Tf:
        INT.step()
        S.append(Get_Sase( INT.y, Range, 855))
        L.append(Get_Laser( INT.y, Range, 855))
        T.append(INT.t)
        #if n%1000==0: print(n, INT.t/Tf)
        n+=1
    return INT.y, S, L, T

def Radau(Range,Y0, Phi, DPhi, Tf, tmax):
    S = []; S.append(Get_Sase(Y0, Range, 855))
    L = []; L.append(Get_Laser(Y0, Range, 855))
    T = []; T.append(0)
    INT = sp.integrate.Radau(fun=Phi,t0=0,y0=Y0,t_bound=Tf, max_step=0.1*tmax, jac=DPhi) #rtol=1e-14 
    n = 0
    while INT.t<Tf:
        INT.step()
        S.append(Get_Sase( INT.y, Range, 855))
        L.append(Get_Laser( INT.y, Range, 855))
        T.append(INT.t)
        #if n%100==0: print(n, INT.t/Tf)
        n+=1
    return INT.y, S, L, T
    









