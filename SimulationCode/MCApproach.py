###############################################################################
###     BACHELOR THESIS                                                     ###
###     IVP SIMULATION OF XRAY PUMPED NEON LASER                            ###
###     MORITZ TANNER 2024                                                  ###
###     ETHZ & PSI                                                          ###
###############################################################################

import numpy as np
import matplotlib.pyplot as plt

def Smooth_Detector(S, n):
    gauss = 1/np.sqrt(2*np.pi)/(n/2.2)*np.exp(-(np.arange(20)-10)**2/(n/2.2)**2)
    return np.convolve(S, np.ones(n)/n, "same")

def Extract_SASE(Range,S,E):
    return np.amax(S[(Range>E-5)*(Range<E+5)])

def Extract_Laser(Range,S):
    return np.amax(S[(Range>847)*(Range<850)])

    
def Make_Kappa(Range, Xpeak, Ypeak, Width):
    kappa =  1/(np.pi*Width*(1+((Range-Xpeak)/Width)**2))
    return kappa / np.amax(kappa) * Ypeak

def Make_Sigma(Range, Xpeak, Ypeak, Speed):
    return Ypeak/(1+np.exp(-Speed*(Range-Xpeak)))

def Make_Gamma(Range, Xpeak, Ypeak, Width):
    return Ypeak/(np.pi*Width*(1+((Range-Xpeak)/Width)**2))

def Make_SASE(Range, Mean, Var, Intensity):
    sase = np.exp( -(Range-Mean)**2 / Var**2)
    sase = sase/ sase.sum()
    SASE = np.random.binomial(Intensity, sase)
    return SASE

###Assumptions:
# Every Photon can only interact with 1 atom
# Until all atoms have interacted with photons, no duplicates


def MonteCarlo(S,N,Sigma,Kappa,Gamma,alpha,beta):
    for t in range(1,S.shape[0]):
        ### Declare Local Variables
        F = S[t-1,:-6]; G = S[t-1,-6]; H = S[t-1,-5]
        ### Sample Ionization
        ion = np.random.binomial(F, 1-np.exp(-Sigma*G))                      
        Ion = G*(1-np.exp(-ion.sum()/G))                          
        ION = (ion/ion.sum()*Ion).astype(np.int64)
        ### Sample Stimulated Emission
        sti = np.random.binomial(F, 1-np.exp(-Kappa*H)) 
        Sti = H*(1-np.exp(-sti.sum()/H))                      
        STI = (sti/sti.sum()*Sti).astype(np.int64)        
        ### Sample Decay 
        DEC = np.random.binomial(H-STI.sum(), beta)                
        ### Sample Spontanious Emission
        SPO = np.random.binomial(DEC*(1+F*0), Gamma).astype(np.int64)
        ### Fill in S at next t
        S[t,:-6]    = np.abs(F - ION + STI + SPO)
        S[t,-6]     = np.abs((1-alpha)*G + alpha*N - ION.sum())
        S[t,-5]     = np.abs((1-alpha)*H + ION.sum() - STI.sum() - DEC)
        S[t,-4]     = ION.sum()
        S[t,-3]     = STI.sum()
        S[t,-2]     = DEC
        S[t,-1]     = SPO.sum()
    return S

    

    





    
    
    
    
