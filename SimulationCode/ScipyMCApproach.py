###############################################################################
###     BACHELOR THESIS                                                     ###
###     IVP SIMULATION OF XRAY PUMPED NEON LASER                            ###
###     MORITZ TANNER 2024                                                  ###
###     ETHZ & PSI                                                          ###
###############################################################################

import numpy as np
import scipy as sp

def Make_Kappa(Range, Xpeak, Ypeak, Width):
    kappa =  1/(np.pi*Width*(1+((Range-Xpeak)/Width)**2))
    kappa = kappa / np.amax(kappa) * Ypeak
    return kappa

def Make_Sigma(Range, Xpeak, Ypeak, Speed):
    return Ypeak/(1+np.exp(-Speed*(Range-Xpeak)))

def Make_Gamma(Range,alpha,beta,dt,gamma,mu,las):
    def Gamma(X,x):
        dH  = dt*beta*((X[-1]-x[-1]) - (X[:-2]-x[:-2]).sum())/(1+dt*(alpha+beta))
        P   = np.random.binomial( dH, mu* 1/(np.pi*gamma*(1+((Range-las)/gamma)**2)) )
        return P
    return Gamma

def Make_SASE(Range, Mean, Var, Intensity):
    sase = np.exp( -(Range-Mean)**2 / Var**2)
    sase = sase/ sase.sum()
    SASE = np.random.binomial(Intensity, sase)
    return SASE


def Make_Phi(N,alpha,beta,sigma,kappa,gamma,Volume):
    def Phi(X):
        V = np.zeros(X.shape[0])
        V[:-2]  = dt*(kappa*X[-1] - sigma*X[-2])*X[:-2]/Volume
        V[-2]   = alpha*N - alpha*X[-2] - dt*((sigma*X[:-2]).sum()*X[-2]/Volume )
        V[-1]   = -(alpha + beta)*X[-1] + dt*(((sigma*X[-2] - kappa*X[-1])*X[:-2]).sum()/Volume)
        return V
    return Phi


#florecence probability mu, FWHM gamma, mean las.



### Simulation Function of 1 Shot
def IVP(Y,Phi,Gamma):
    ### Iteration of Implicit Euler Scheme
    for t in range(1,Y.shape[0]):           
        ### Explicit Euler Guess
        Y[t,:] = Y[t-1,:] + Phi(Y[t-1,:])
        ### Newton-Raphson Method
        #Y[t,:] = (sp.optimize.newton(lambda x: x - Y[t-1,:] - Phi(x), X))

        #print(Y[t,:]- Y[t-1,:] - Phi(Y[t,:]))
        #break
        ### Add spontanious emission
        #Y[t,:-2] += Gamma(Y[t,:],Y[t-1,:])
    return Y





