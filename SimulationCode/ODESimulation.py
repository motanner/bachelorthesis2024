import numpy as np
import scipy as sp

# Returns Constructed Phi Function (Right Hand Side ODE Opperator)
def Make_Phi(N, Alpha,Beta,Gamma,Kappa,Sigma):
    def Phi(t,Y):
        G,H,I,J = Y[0], Y[1], Y[2], Y[3]
        return np.asarray([
            -Alpha*G + Alpha*N - Sigma*I*G,
            -Alpha*H - Beta*H  + Sigma*I*G - Kappa*J*H,
            -Sigma*I*G,
            Kappa*J*H + Gamma*Beta*H
            ])
    return Phi

# Return Jacobian of Phi for implicit Runge Kutta Methods
def Make_DPhi(Alpha,Beta,Gamma,Kappa,Sigma):
    def DPhi(t,Y):
        G,H,I,J =  Y[0], Y[1], Y[2], Y[3]
        return np.asarray([
            [-Alpha-Sigma*I, 0, -Sigma*G, 0],
            [Sigma*I, -Alpha-Beta-Kappa*J, Sigma*G, -Kappa*H],
            [-Sigma*I, 0, -Sigma*G, 0],
            [0, Kappa*J+Gamma*Beta, 0, Kappa*H]
            ])
    return DPhi

# Executes ODE Integrator (Implicit if DPhi is given)
def RUN(X, T, t, Phi, DPhi=False):
    G,H,I,J,K = [X[0]], [X[1]], [X[2]], [X[3]], [0]
    if DPhi == False:
        INT = sp.integrate.RK45(fun=Phi, t0=0, y0=X, t_bound=T, max_step=0.1*t)
    else:
        INT = sp.integrate.Radau(fun=Phi,t0=0,y0=X,t_bound=T, max_step=0.1*t, jac=DPhi)
    while INT.t < T:
        INT.step()
        G.append(INT.y[0])
        H.append(INT.y[1])
        I.append(INT.y[2])
        J.append(INT.y[3])
        K.append(INT.t)
    return G,H,I,J,K
