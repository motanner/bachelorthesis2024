### Create compressed Arrays

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
import warnings
import time
warnings.simplefilter('ignore')

# Load Compressed Data
DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/Data.npy", allow_pickle=True).item()

# Callibration Functions

an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083

S = 100

OUT = {}

# calculate the key values for the shots and store them in COMP file

for RUN,START in [("19",15),("20",15),("21",15),("15",0),("90",0)]:
    OMIN, OMAX, OABS, OARG, OFWHM, OAE, OBE, ONE, OFE, OVAL,OBKG = [],[],[],[],[],[],[],[],[],[],[]
    for i, (XV,XN,XE) in enumerate(DATA[RUN][START:]):

        
        
        RN = XN[:,1000:1500].mean(axis=1)
        CE = XE.cumsum(axis=1)
        SE = ( CE[:, S:] - CE[:, :-S] ) / S

        ME = np.amax(SE, axis=1)
        CE = np.sort(SE, axis=1)[:, SE.shape[1]//2]
        FE = (SE>(CE+(ME-CE)/2).reshape(SE.shape[0],1)).sum(axis=1)
        NE = np.argmax(SE, axis=1) + S//2 + 1
        QE = (SE>(CE+(ME-CE)/10).reshape(SE.shape[0],1))
        AE = np.argmax(QE, axis=1) + S//2 + 1
        BE = XE.shape[1] - np.argmax(np.flip(QE, axis=1), axis=1) - S//2

        MIN = np.amin(XN[:,900:1100], axis=1)
        MAX = np.amax(XN[:,980:1020], axis=1)

        Val = np.ones(XN.shape[0])*float(XV)
        
        #ARG = np.argmax(N[:,980:1020], axis=1)
        
        FWHM = ( XN[:,950:1050] > ((MAX-MIN)/2+MIN).reshape(MAX.shape[0],1) ).sum(axis=1)
        Abs = np.amax(XN[:,:900], axis=1)
        #print(AE[0], BE[0])
        #plt.plot(XE[0,:]); plt.plot(S//2 + np.arange(SE.shape[1]), SE[0,:]); plt.show()
        """
        for k in range(20):
            fig,(ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(11,8))
            ax1.plot(XN[k,:]);
            ax1.vlines((AE[k]-bp)/ap, 0, np.amax(XN[k,:]), "orange")
            ax1.vlines((BE[k]-bp)/ap, 0, np.amax(XN[k,:]), "orange")
            ax1.vlines((NE[k]-bp)/ap, 0, np.amax(XN[k,:]), "red")
            ax2.plot(XE[k,:])
            ax2.plot(range(S//2+1,S//2+1+SE.shape[1]),SE[k], "purple")
            ax2.vlines(NE[k], 0, np.amax(XE[k,:]), "red")
            ax2.vlines(BE[k], 0, np.amax(XE[k,:]), "orange")
            ax2.vlines(AE[k], 0, np.amax(XE[k,:]), "orange")
            sec =  ax1.secondary_xaxis('top', functions=(lambda x: (x-bn)/an, lambda x: an*x+bn))
            plt.show()
        """
        OMIN.append(MIN)    #
        OMAX.append(MAX)    #
        OFWHM.append(FWHM)  #
        OAE.append(AE)      #
        OBE.append(BE)      #
        ONE.append(NE)      #
        OFE.append(FE)      #
        OVAL.append(Val)
        OABS.append(Abs)
        OBKG.append(RN)
        print(RUN,i)
    out = {}
    out["MIN"] = np.concatenate(OMIN)   #maximum height of L
    out["MAX"] = np.concatenate(OMAX)   #minimum height of L
    out["ARG"] = np.concatenate(ONE)    #position peak of E
    out["WID"] = np.concatenate(OFWHM)  #width of L, FWHM
    out["LEN"] = np.concatenate(OFE)    #width of E, FWHM
    out["EQA"] = np.concatenate(OAE)    #start of E spec (quarter height)
    out["EQB"] = np.concatenate(OBE)    #end of E spec (quarter height)
    out["VAL"] = np.concatenate(OVAL)   #either target EV or Pressure
    out["ABS"] = np.concatenate(OABS)   #Max intensity of SASE pulse in outgoing spectrum
    out["BKG"] = np.concatenate(OBKG)
    
    OUT[RUN] = out

np.save("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/COMP.npy", OUT, allow_pickle=True)

