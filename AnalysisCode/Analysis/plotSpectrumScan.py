### Full Energy-spectrum images

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
import warnings
import time
warnings.simplefilter('ignore')

# Load Compressed Data
DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/Data.npy", allow_pickle=True).item()

# Callibration Functions
an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083
def EtoEV(pixel): return (((pixel- bp) / ap ) - bn ) / an
def NtoEV(pixel): return (pixel - bn) / an

#Create Images

fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(10, 8))
S = 50
Mmin = 800
Mmax = 1350
M = 100
Mlen = (Mmax-Mmin)/M
N = DATA["19"][0][1].shape[1]

#Creates the Spectrum Scan Images for 19,20,21,22
for idx, (NAME,RUN) in enumerate([("A","19"), ("B","20"), ("C","21"), ("D","22")]):
    PIC = np.zeros((M,N))
    Count = np.zeros((M,1))
    for  I, (XV,XN,XE) in enumerate(DATA[RUN]):
        CE = XE.cumsum(axis=1)
        SE = ( CE[:, S:] - CE[:, :-S] ) / S
        CE = np.sort(SE, axis=1)[:, SE.shape[1]//2]
        NE = np.argmax(SE, axis=1) + S//2 + 1
        for mi in range(M):
            J = mi*Mlen+Mmin; RANGE = (NE>J)*(NE<J+Mlen)
            PIC[mi, :] += XN[RANGE,:].sum(axis=0)
            Count[mi, 0] += RANGE.sum()
    print(RUN, idx//2, idx % 2)
    AX = axs[idx//2][idx % 2]
    AX.pcolormesh(PIC/Count)
    AX.set_title(NAME)
    AX.set_xlabel("Spectrum: [eV]")
    AX.set_ylabel("Beam Energy [eV]")
    AX.set_xticks(np.arange(N)[::201])
    AX.set_xticklabels( [f"{round(tick)}" for tick in ((np.arange(N) - bn)/an)[::201]] )
    AX.set_yticks(np.arange(M)[::9])
    AX.set_yticklabels([f"{round(tick)}" for tick in  (((( np.arange(M) *Mlen + Mmin +Mlen/2)-bp)/ap - bn)/an)[::9]  ])
fig.tight_layout()
plt.savefig("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/FULLSPECTRUM.png")
plt.show()



    
