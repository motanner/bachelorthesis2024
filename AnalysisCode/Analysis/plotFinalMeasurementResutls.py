### Create plots

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
import warnings
import time
warnings.simplefilter('ignore')

an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083

# Load Compressed Data
DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/COMP.npy", allow_pickle=True).item()

def EtoEV(x): return ((x-bp)/ap-bn)/an

### PRESSURE 
D15 = DATA["15"]; D90 = DATA["90"]

U15 = np.unique(D15["VAL"][(D15["VAL"]>400)]); U90 = np.unique(D90["VAL"])
V15, V90 = [],[]
A15,A90 = [],[]
P15,P90 = [],[]
Lavg15,Lavg90 = [],[]
Lstd15lo,Lstd90lo = [],[]
Lstd15hi,Lstd90hi = [],[]
Aavg15,Aavg90 = [],[]
Astd15lo,Astd90lo = [],[]
Astd15hi,Astd90hi = [],[]
L15max,L15min = [],[]
L90max,L90min = [],[]
A15max,A15min = [],[]
A90max,A90min = [],[]
T90,T15 = [],[]
for v in U15: V15.append( (D15["MAX"]-D15["BKG"])[D15["VAL"]==v] )
for v in U90: V90.append( (D90["MAX"]-D90["BKG"])[D90["VAL"]==v] )
for v in U15: A15.append( (D15["ABS"]-D15["BKG"])[D15["VAL"]==v] )
for v in U90: A90.append( (D90["ABS"]-D90["BKG"])[D90["VAL"]==v] )

def Range15(v): return (D15["VAL"]==v)*( (D15["MAX"]-D15["BKG"]) >= 1.2*(D15["BKG"]-D15["MIN"]))
def Range90(v): return (D90["VAL"]==v)*( (D90["MAX"]-D90["BKG"]) >= 1.2*(D90["BKG"]-D90["MIN"]))

J15 = []
J90 = []

for v in U15:
    if Range15(v).sum()>0: J15.append(v)  
for v in U90:
    if Range90(v).sum()>0: J90.append(v) 



for v in J15: Lavg15.append( np.nanquantile((D15["MAX"]-D15["BKG"])[Range15(v)],0.5) ) 
for v in J90: Lavg90.append( np.nanquantile((D90["MAX"]-D90["BKG"])[Range90(v)],0.5) ) 
for v in J15: Lstd15lo.append( np.nanquantile((D15["MAX"]-D15["BKG"])[Range15(v)],0.25) )
for v in J90: Lstd90lo.append( np.nanquantile((D90["MAX"]-D90["BKG"])[Range90(v)],0.25) )  
for v in J15: Lstd15hi.append( np.nanquantile((D15["MAX"]-D15["BKG"])[Range15(v)],0.75) )  
for v in J90: Lstd90hi.append( np.nanquantile((D90["MAX"]-D90["BKG"])[Range90(v)],0.75) )  
for v in J15: L15max.append( np.amax((D15["MAX"]-D15["BKG"])[Range15(v)]) )  
for v in J90: L90max.append( np.amax((D90["MAX"]-D90["BKG"])[Range90(v)]) )  
for v in J15: L15min.append( np.amin((D15["MAX"]-D15["BKG"])[Range15(v)]) ) 
for v in J90: L90min.append( np.amin((D90["MAX"]-D90["BKG"])[Range90(v)]) )  

for v in U15: Aavg15.append( np.quantile((D15["ABS"]-D15["BKG"])[D15["VAL"]==v],0.5) )
for v in U90: Aavg90.append( np.quantile((D90["ABS"]-D90["BKG"])[D90["VAL"]==v],0.5) )
for v in U15: Astd15lo.append( np.quantile((D15["ABS"]-D15["BKG"])[D15["VAL"]==v],0.25) )
for v in U90: Astd90lo.append( np.quantile((D90["ABS"]-D90["BKG"])[D90["VAL"]==v],0.25) )
for v in U15: Astd15hi.append( np.quantile((D15["ABS"]-D15["BKG"])[D15["VAL"]==v],0.75) )
for v in U90: Astd90hi.append( np.quantile((D90["ABS"]-D90["BKG"])[D90["VAL"]==v],0.75) )
for v in U15: A15max.append( np.amax((D15["ABS"]-D15["BKG"])[D15["VAL"]==v]) )
for v in U90: A90max.append( np.amax((D90["ABS"]-D90["BKG"])[D90["VAL"]==v]) )
for v in U15: A15min.append( np.amin((D15["ABS"]-D15["BKG"])[D15["VAL"]==v]) )
for v in U90: A90min.append( np.amin((D90["ABS"]-D90["BKG"])[D90["VAL"]==v]) )

for v in U15: T15.append( 1.2* np.mean((D15["BKG"]-D15["MIN"])[D15["VAL"]==v]) )
for v in U90: T90.append( 1.2* np.mean((D90["BKG"]-D90["MIN"])[D90["VAL"]==v]) )

for i,v in enumerate(U15): P15.append( ((D15["MAX"]-D15["BKG"])[D15["VAL"]==v] >= 1.2*(D15["BKG"]-D15["MIN"])[D15["VAL"]==v]).sum() / ((D15["MAX"]-D15["BKG"])[D15["VAL"]==v] >= -100).sum() )
for i,v in enumerate(U90): P90.append( ((D90["MAX"]-D90["BKG"])[D90["VAL"]==v] >= 1.2*(D90["BKG"]-D90["MIN"])[D90["VAL"]==v]).sum() / ((D90["MAX"]-D90["BKG"])[D90["VAL"]==v] >= -100).sum() )
#print(len(Astd90hi), len(Astd90lo), len(Aavg90), len(U90))

fig, ((ax3, ax4),(ax1,ax2)) = plt.subplots(nrows=2, ncols=2, layout='constrained', figsize = (10,8))


ax1.set_title("C")
ax1.plot(U90, T90, linestyle="--", color = "black")
ax1.fill_between(U90, Astd90lo, Astd90hi,alpha=0.3, color="blue", linestyle="--") #, label="SASE Brighness 0.25-0.75 Quantile"
#ax1.plot(U90, A90max, color="blue", linestyle="dotted")
#ax1.plot(U90, A90min, color="blue", linestyle="dotted")
ax1.plot(U90, Aavg90, color="blue", linestyle="-",  label="SASE Brightness")
ax1.fill_between(J90, Lstd90lo, Lstd90hi,alpha=0.3, color="green",linestyle="--") #, label="Lasing Brightness 0.25-0.75 Quantile"
ax1.plot(J90, Lavg90, color="green", linestyle="-",  label="Lasing Brightness")
#ax1.plot(U90, L90max, color="green", linestyle="dotted")
#ax1.plot(U90, L90min, color="green", linestyle="dotted")

ax1.set_xlabel("Pressure [mBar]")
ax1.set_ylabel("Brightness [arb. unit]")
sec1 =  ax1.secondary_yaxis('right', functions=(lambda x: x/np.amax(Astd90hi), lambda x: np.amax(Astd90hi)*x))
sec1.set_ylabel("Lasing Probability")
ax1.plot(U90, np.amax(Astd90hi)*np.asarray(P90), color = "red", label= "Probability Lasing", linestyle = ":")
#ax1.legend()

ax2.set_title("D")
ax2.plot(U15, T15, linestyle="--", color = "black")
ax2.fill_between(U15, Astd15lo, Astd15hi,alpha=0.3, color="blue", linestyle="--")
#ax2.plot(U15, A15min, color="blue", linestyle="dotted")
#ax2.plot(U15, A15max, color="blue", linestyle="dotted")
ax2.plot(U15, Aavg15, color="blue", linestyle="-",  label="SASE Median Brighness")
ax2.fill_between(J15, Lstd15lo, Lstd15hi,alpha=0.3, color="green",linestyle="--")
ax2.plot(J15, Lavg15, color="green", linestyle="-",  label="Lasing Median Brighness")
#ax2.plot(U15, L15max, color="green", linestyle="dotted")
#ax2.plot(U15, L15min, color="green", linestyle="dotted")
ax2.set_xlabel("Pressure [mBar]")
ax2.set_ylabel("Brightness [arb. unit]")
sec2 =  ax2.secondary_yaxis('right', functions=(lambda x: x/np.amax(Lstd15hi), lambda x: np.amax(Lstd15hi)*x))
sec2.set_ylabel("Lasing Probability")
ax2.plot(U15, np.amax(Lstd15hi)*np.asarray(P15), color = "red", label= "Probability Lasing", linestyle = ":")
#ax2.legend()

#plt.tight_layout()
#plt.show()










### ENERGY
D19,D20,D21 = DATA["19"], DATA["20"], DATA["21"]

#Lasing, Absorbtion, Width, Probability 870 uJ, 300 uJ
L300a,L300b,L300c,L870a,L870b,L870c  = [],[],[],[],[],[]
A300a,A300b,A300c,A870a,A870b,A870c  = [],[],[],[],[],[]
W300a,W300b,W300c,W870a,W870b,W870c  = [],[],[],[],[],[]
P300,P870  = [],[]
T300,T870 = [],[]
L3min,L3max,A3min,A3max = [],[],[],[]
L8min,L8max,A8min,A8max = [],[],[],[]

Steps = 20
U300 = np.arange(np.amin(D21["ARG"]), np.amax(D21["ARG"]), (np.amax(D21["ARG"]) - np.amin(D21["ARG"])) / (Steps+1))
U870 = np.arange(min(np.amin(D19["ARG"]),np.amin(D20["ARG"])),max( np.amax(D19["ARG"]), np.amax(D20["ARG"])), (max( np.amax(D19["ARG"]), np.amax(D20["ARG"])) - min(np.amin(D19["ARG"]),np.amin(D20["ARG"]))) /(Steps+1))
u300 = EtoEV((U300[1:]+U300[:-1])/2) 
u870 = EtoEV((U870[1:]+U870[:-1])/2)
j3 = []
j8 = []

for v in range(Steps):
    Range = (D21["ARG"] >= U300[v])* (D21["ARG"] <= U300[v+1])
    Las = (D21["MAX"]-D21["BKG"])[Range]
    Bkg = (D21["BKG"]-D21["MIN"])[Range]
    Abs = (D21["ABS"]-D21["BKG"])[Range]
    Wid = D21["WID"][Range]
    LAS = Las[Las>=1.2*Bkg]
    WID = Wid[Las>=1.2*Bkg]
    if (Las>=1.2*Bkg).sum()>0:
        j3.append(u300[v])
        L300a.append( np.quantile(LAS, 0.25));
        L300b.append( np.quantile(LAS, 0.5));
        L300c.append( np.quantile(LAS, 0.75))
        W300a.append( np.quantile(WID, 0.25));
        W300b.append( np.quantile(WID, 0.5));
        W300c.append( np.quantile(WID, 0.75))
    A300a.append( np.quantile(Abs, 0.25)); A300b.append( np.quantile(Abs, 0.5)); A300c.append( np.quantile(Abs, 0.75))
    P300.append( (Las >= 1.2*Bkg).sum() / (Las >= -100).sum())
    T300.append( 1.2*Bkg.mean())
    L3min.append( np.amin(Las))
    L3max.append( np.amax(Las))
    A3min.append( np.amin(Abs))
    A3max.append( np.amax(Abs))
    

for v in range(Steps):
    Range19 = (D19["ARG"] >= U870[v])* (D19["ARG"] <= U870[v+1])
    Range20 = (D20["ARG"] >= U870[v])* (D20["ARG"] <= U870[v+1])
    Las = np.concatenate( ( (D19["MAX"]-D19["BKG"])[Range19], (D20["MAX"]-D20["BKG"])[Range20]))
    Bkg = np.concatenate( ( (D19["BKG"]-D19["MIN"])[Range19], (D20["BKG"]-D20["MIN"])[Range20]))
    Abs = np.concatenate( ( (D19["ABS"]-D19["BKG"])[Range19], (D20["ABS"]-D20["BKG"])[Range20]))
    Wid = np.concatenate( ( D19["WID"][Range19], D20["WID"][Range20]))
    LAS = Las[Las>=1.2*Bkg]
    WID = Wid[Las>=1.2*Bkg]

    if (Las>=1.2*Bkg).sum()>0:
        j8.append(u870[v])
        L870a.append( np.quantile(LAS, 0.25)); L870b.append( np.quantile(LAS, 0.5)); L870c.append( np.quantile(LAS, 0.75))
        W870a.append( np.quantile(WID, 0.25)); W870b.append( np.quantile(WID, 0.5)); W870c.append( np.quantile(WID, 0.75))
    A870a.append( np.quantile(Abs, 0.25)); A870b.append( np.quantile(Abs, 0.5)); A870c.append( np.quantile(Abs, 0.75))
    P870.append( (Las >= 1.2*Bkg).sum() / (Las >= -100).sum())
    T870.append( 1.2*Bkg.mean())
    L8min.append( np.amin(Las))
    L8max.append( np.amax(Las))
    A8min.append( np.amin(Abs))
    A8max.append( np.amax(Abs))

#fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, layout='constrained', figsize = (11,8))
#fig.suptitle("Energy Analysis in Bands")

ax3.set_title("A")
ax3.plot(u870, T870, linestyle="--", color = "black")
ax3.fill_between(u870, A870a, A870c,alpha=0.3, color="blue", linestyle="--")
ax3.plot(u870, A870b, color="blue", linestyle="-",  label="SASE Brightness")
#ax1.plot(u870, A8min, color="blue", linestyle="dotted")
#ax1.plot(u870, A8max, color="blue", linestyle="dotted")
ax3.fill_between(j8, L870a, L870c,alpha=0.3, color="green",linestyle="--")
ax3.plot(j8, L870b, color="green", linestyle="-",  label="Lasing Brightness")
#ax1.plot(u870, L8min, color="green", linestyle="dotted")
#ax1.plot(u870, L8max, color="green", linestyle="dotted")
ax3.fill_between(j8, W870a, W870c,alpha=0.3, color="orange",linestyle="--")
ax3.plot(j8, W870b, color="orange", linestyle="-",  label="Lasing Width")
ax3.set_xlabel("SASE Energy [eV]")
ax3.set_ylabel("Brightness [arb. unit] & Width [Pixel]")
sec3 =  ax3.secondary_yaxis('right', functions=(lambda x: x/np.amax(A870c), lambda x: np.amax(A870c)*x))
sec3.set_ylabel("Lasing Probability")
ax3.plot(u870, np.amax(A870c)*np.asarray(P870), color = "red", label= "Probability Lasing" , linestyle = ":")
#ax3.legend()

ax4.set_title("B")
ax4.plot(u300, T300, linestyle="--", color = "black")
ax4.fill_between(u300, A300a, A300c, alpha=0.3, color="blue", linestyle="--")
ax4.plot(u300, A300b, color="blue", linestyle="-",  label="SASE Brightness")
#ax2.plot(u300, A3min, color="blue", linestyle="dotted")
#ax2.plot(u300, A3max, color="blue", linestyle="dotted")
ax4.fill_between(j3, L300a, L300c,alpha=0.3, color="green",linestyle="--")
ax4.plot(j3, L300b, color="green", linestyle="-",  label="Lasing Brightness")
ax4.fill_between(j3, W300a, W300c,alpha=0.3, color="orange",linestyle="--")
#ax2.plot(u300, L3min, color="green", linestyle="dotted")
#ax2.plot(u300, L3max, color="green", linestyle="dotted")
ax4.plot(j3, W300b, color="orange", linestyle="-",  label="Lasing Width")
ax4.set_xlabel("SASE Energy [eV]")
ax4.set_ylabel("Brightness [arb. unit] & Width [Pixel]")
sec4 =  ax4.secondary_yaxis('right', functions=(lambda x: x/np.amax(A300c), lambda x: np.amax(A300c)*x))
sec4.set_ylabel("Lasing Probability")
ax4.plot(u300, np.amax(A300c)*np.asarray(P300), color = "red", label= "Probability Lasing", linestyle = ":")
ax4.legend()

plt.tight_layout()
plt.show()










fig, (ax1) = plt.subplots(nrows=1, ncols=1, layout='constrained', figsize = (11,8))
fig.suptitle("Energy Analysis: Width vs Brightness {Dataset 19+20}")

Las = np.concatenate((D19["MAX"]-D19["BKG"], D20["MAX"]-D20["BKG"]))
Wid = np.concatenate((D19["WID"], D20["WID"]))
Bkg = np.concatenate((D19["BKG"]-D19["MIN"], D20["BKG"]-D20["MIN"]))
Range = (Las >= 1.2*Bkg)
Random = np.random.normal(loc=0, scale=0.2, size=Range.sum())

                     

pic1 = ax1.scatter( Las[Range] , Wid[Range] + Random,
                    s=0.2, cmap="gist_rainbow", c=np.concatenate(( EtoEV(D19["ARG"]), EtoEV(D20["ARG"])))[Range] )
cbar1 = fig.colorbar(pic1, ax=ax1, location="right")
cbar1.set_label("SASE Energy [eV]")
ax1.set_xlabel("Lasing Width [Pixel]")
ax1.set_ylabel("Lasing Brightness [Arbitrary Units]")


plt.show()

print(EtoEV(850)-EtoEV(840))





