

### Create plots

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
import warnings
import time
warnings.simplefilter('ignore')

an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083

# Load Compressed Data
DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/FINAL.nosync/Data.npy", allow_pickle=True).item()

S = 100
def EtoEV(X): return ((np.argmax(( X.cumsum(axis=1)[:, S:] - X.cumsum(axis=1)[:, :-S]) / S, axis=1) + S//2 + 1 - bp)/ap - bn)/an

fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(11,8))

### 19 & 20

M,Mmin,Mmax = 100,((800-bp)/ap-bn)/an,((1350-bp)/ap-bn)/an
Mlen = (Mmax-Mmin)/M
N = DATA["19"][0][1].shape[1]

PIC = np.zeros((M,N))
Count = np.zeros((M,1))

for  idx, ((V19, N19, E19),(V20,N20,E20)) in enumerate(zip(DATA["19"], DATA["20"])):
    EV19 = EtoEV(E19); EV20 = EtoEV(E20)
    for mi in range(M):
        R19 = (EV19>mi*Mlen+Mmin)*(EV19<mi*Mlen+Mmin+Mlen)
        R20 = (EV20>mi*Mlen+Mmin)*(EV20<mi*Mlen+Mmin+Mlen)
        PIC[mi,:] += N19[R19,:].sum(axis=0)[::-1] + N20[R20,:].sum(axis=0)[::-1]
        Count[mi,0] += R19.sum() + R20.sum()
axs[0,0].pcolormesh(PIC/Count)
#axs[0,0].set_title("Energy Scan 870 uJ {Dataset 19 + 20}")
axs[0,0].set_title("A")
axs[0,0].set_xlabel("Spectrum [eV]")
axs[0,0].set_ylabel("Beam Energy [eV]")
axs[0,0].set_xticks(np.arange(N)[::201])
axs[0,0].set_xticklabels( [f"{round(tick)}" for tick in ((np.arange(N)[::-1] - bn)/an)[::201]] )
axs[0,0].set_yticks(np.arange(M)[::9])
axs[0,0].set_yticklabels([f"{round(tick)}" for tick in  ( np.arange(M) *Mlen + Mmin +Mlen/2)[::9]  ])



### 21

PIC = np.zeros((M,N))
Count = np.zeros((M,1))

for  idx, (V21, N21, E21) in enumerate(DATA["21"]):
    EV21 = EtoEV(E21)
    for mi in range(M):
        R21 = (EV21>mi*Mlen+Mmin)*(EV21<mi*Mlen+Mmin+Mlen)
        PIC[mi,:] += N21[R21,:].sum(axis=0)[::-1] 
        Count[mi,0] += R21.sum() 
axs[0,1].pcolormesh(PIC/Count)
#axs[0,1].set_title("Energy Scan 300 uJ {Dataset 21}")
axs[0,1].set_title("B")
axs[0,1].set_xlabel("Spectrum [eV]")
axs[0,1].set_ylabel("Beam Energy [eV]")
axs[0,1].set_xticks(np.arange(N)[::201])
axs[0,1].set_xticklabels( [f"{round(tick)}" for tick in ((np.arange(N)[::-1] - bn)/an)[::201]] )
axs[0,1].set_yticks(np.arange(M)[::9])
axs[0,1].set_yticklabels([f"{round(tick)}" for tick in  ( np.arange(M) *Mlen + Mmin +Mlen/2)[::9]  ])


### 90

M = len(DATA["90"])
PIC = np.zeros((M,N))
Count = np.zeros((M,1))
Ticks = []

for  idx, (V90, N90, E90) in enumerate(DATA["90"]):
    PIC[idx,:] += N90.sum(axis=0)[::-1] 
    Count[idx,0] += N90.shape[0]
    Ticks.append(V90)
axs[1,0].pcolormesh(PIC/Count)
#axs[1,0].set_title("Pressure Scan 90 fs {Dataset 90}")
axs[1,0].set_title("C")
axs[1,0].set_xlabel("Spectrum [eV]")
axs[1,0].set_ylabel("Neon Pressure [mBar]")
axs[1,0].set_xticks(np.arange(N)[::201])
axs[1,0].set_xticklabels( [f"{round(tick)}" for tick in ((np.arange(N)[::-1] - bn)/an)[::201]] )
axs[1,0].set_yticks(np.arange(M)[::2])
axs[1,0].set_yticklabels(Ticks[::2])

### 15

M = len(DATA["15"])
PIC = np.zeros((M,N))
Count = np.zeros((M,1))
Ticks = []

for  idx, (V15, N15, E15) in enumerate(DATA["15"]):
    PIC[idx,:] += N15.sum(axis=0)[::-1] 
    Count[idx,0] += N15.shape[0]
    Ticks.append(V15)
axs[1,1].pcolormesh(PIC/Count)
#axs[1,1].set_title("Pressure Scan 15 fs {Dataset 15}")
axs[1,1].set_title("D")
axs[1,1].set_xlabel("Spectrum [eV]")
axs[1,1].set_ylabel("Neon Pressure [mBar]")
axs[1,1].set_xticks(np.arange(N)[::201])
axs[1,1].set_xticklabels( [f"{round(tick)}" for tick in ((np.arange(N)[::-1] - bn)/an)[::201]] )
axs[1,1].set_yticks(np.arange(M)[::2])
axs[1,1].set_yticklabels(Ticks[::2])


plt.tight_layout()
plt.show()        




