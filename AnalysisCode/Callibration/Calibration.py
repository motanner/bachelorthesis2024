import numpy as np
from matplotlib import pyplot as plt

#Pixels: 734, 750, 1004
#Values: 868.6, 867.12, 848.7

##NEON: -0.07394502876401877 922.7317438549595
#Linear Fit NEON PIXELS to EV: A*x+B : A = -0.0739, B = 922.7317

##ELECTRON 0.04189572884970152 823.001776407745
#TRANSLATED: ELECTRON PIXELS to EV: A*x+B : A = 0.0419 , B = 823.0018


NAMES = ["19", "20", "21", "22"]

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]
    EV[name] = []; Ev[name] = SCAN[0]
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        mE = np.mean(rE, axis=0)

        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        aE = np.argmax(mE)
        
        print(k,aN, aE)

        plt.title(k)
        plt.plot(mN)
        plt.axvline(x=aN, color='r', linestyle='--')
        plt.axvline(x=a-k*12, color='g', linestyle='--')
        plt.axvline(x=b-k*12, color='g', linestyle='--')
        plt.show()
        #plt.plot(mE)
        #plt.axvline(x=aE, color='r', linestyle='--')
        #plt.show()
        
        NV[name].append(aN)
        EV[name].append(aE)



###NEON
plt.figure(figsize=(8,8))

plt.plot(NV["19"], Nv["19"], label = "19", color='red')
plt.plot(NV["20"], Nv["20"], label = "20", color='blue')
plt.plot(NV["21"], Nv["21"], label = "21", color='green')
plt.plot(NV["22"], Nv["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###ELECTRON
plt.figure(figsize=(8,8))

plt.plot(EV["19"], Ev["19"], label = "19", color='red')
plt.plot(EV["20"], Ev["20"], label = "20", color='blue')
plt.plot(EV["21"], Ev["21"], label = "21", color='green')
plt.plot(EV["22"], Ev["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Electron Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###NEON REVERSED
plt.figure(figsize=(8,8))

plt.plot(1840-np.asarray(NV["19"]), Nv["19"], label = "19", color='red')
plt.plot(1840-np.asarray(NV["20"]), Nv["20"], label = "20", color='blue')
plt.plot(1840-np.asarray(NV["21"]), Nv["21"], label = "21", color='green')
plt.plot(1840-np.asarray(NV["22"]), Nv["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()


NAMES = ["19", "20", "21", "22"]

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]
    #EV[name] = []; Ev[name] = SCAN[0]
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        #E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        #rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        #mE = np.mean(rE, axis=0)

        aN = np.argmax(mN)
        #aE = np.argmax(mE)
        """
        print(k,aN, aE)

        plt.plot(mN)
        plt.axvline(x=aN, color='r', linestyle='--')
        plt.axvline(x=a-k*12, color='g', linestyle='--')
        plt.axvline(x=b-k*12, color='g', linestyle='--')
        plt.show()
        plt.plot(mE)
        plt.axvline(x=aE, color='r', linestyle='--')
        plt.show()
        """
        NV[name].append(aN)
        #EV[name].append(aE)

plt.figure(figsize=(8,8))

###NEON Peak

plt.plot(Nv["19"], NV["19"], label = "19", color='red')
plt.plot(Nv["20"], NV["20"], label = "20", color='blue')
plt.plot(Nv["21"], NV["21"], label = "21", color='green')
plt.plot(Nv["22"], NV["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV With Full Gas Cell')
plt.xlabel('eV')
plt.ylabel('Pixels')

# Display the plot
plt.show()


NAMES = ["19", "20", "21", "22"]

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

NV = {}; Nv = {}
EV = {}; Ev = {}
MV = {}; PV = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]
    EV[name] = []; Ev[name] = SCAN[0]
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        mE = np.mean(rE, axis=0)

        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        aE = np.argmax(mE)
        """
        print(k,aN, aE)

        plt.plot(mN)
        plt.axvline(x=aN, color='r', linestyle='--')
        plt.axvline(x=a-k*12, color='g', linestyle='--')
        plt.axvline(x=b-k*12, color='g', linestyle='--')
        plt.show()
        plt.plot(mE)
        plt.axvline(x=aE, color='r', linestyle='--')
        plt.show()
        """
        NV[name].append(aN)
        EV[name].append(aE)
        MV[name] = ( np.argmax(mN) )

        
    NV[name] = np.asarray(NV[name])
    EV[name] = np.asarray(EV[name])
    PV[name] = np.polyfit(NV[name], Nv[name], 1)

print(PV)
print(MV)
shift = {}
for k in PV.keys():
    p = np.poly1d(PV[k])
    shift[name] = p(MV[k]))

###NEON
plt.figure(figsize=(8,8))

plt.plot(NV["19"], Nv["19"], label = "19", color='red')
plt.plot(NV["20"], Nv["20"], label = "20", color='blue')
plt.plot(NV["21"], Nv["21"], label = "21", color='green')
plt.plot(NV["22"], Nv["22"], label = "22", color='purple')

#848.5, 867.12, 
# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###ELECTRON
plt.figure(figsize=(8,8))

plt.plot(EV["19"], Ev["19"], label = "19", color='red')
plt.plot(EV["20"], Ev["20"], label = "20", color='blue')
plt.plot(EV["21"], Ev["21"], label = "21", color='green')
plt.plot(EV["22"], Ev["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Electron Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###NEON REVERSED
plt.figure(figsize=(8,8))

plt.plot(1840-np.asarray(NV["19"]), Nv["19"], label = "19", color='red')
plt.plot(1840-np.asarray(NV["20"]), Nv["20"], label = "20", color='blue')
plt.plot(1840-np.asarray(NV["21"]), Nv["21"], label = "21", color='green')
plt.plot(1840-np.asarray(NV["22"]), Nv["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()
        

NAMES = ["19", "20", "21"]

Min0a,Min0b = 710,730
Min1a,Min1b = 740,760
Min2a,Min2b = 950,1050

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

RESULTS = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    N0, N1 ,N2= [],[],[]
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        mE = np.mean(rE, axis=0)
        #find peaks
        N0.append(mN[Min0a:Min0b])
        N1.append(mN[Min1a:Min1b])
        N2.append(mN[Min2a:Min2b])

    RESULTS[name] = (N0,N1,N2)


for name in NAMES:
    plt.title(f"0 {name}")
    for k in range(25,33):
        LINE = RESULTS[name][0][k]
        LineMin = np.amin(LINE)
        LineMax = np.amax(LINE)
        LINE = (LINE-LineMin) / (LineMax - LineMin)
        plt.plot(LINE, "b")
    plt.show()

for name in NAMES:
    plt.title(f"1 {name}")
    for k in range(25,33):
        LINE = RESULTS[name][1][k]
        LineMin = np.amin(LINE)
        LineMax = np.amax(LINE)
        LINE = (LINE-LineMin) / (LineMax - LineMin)
        plt.plot(LINE, "b")
    plt.show()

for name in NAMES:
    plt.title(f"2 {name}")
    for k in range(22,33):
        LINE = RESULTS[name][2][k]
        LineMin = np.amin(LINE)
        LineMax = np.amax(LINE)
        LINE = (LINE-LineMin) / (LineMax - LineMin)
        plt.plot(LINE, "b")
    plt.show()


#Pixels: 734, 750, 1004
#Values: 868.6, 867.12, 848.7

##NEON: -0.07394502876401877 922.7317438549595
#Linear Fit NEON PIXELS to EV: A*x+B : A = -0.0739, B = 922.7317

##ELECTRON 0.04189572884970152 823.001776407745
#TRANSLATED: ELECTRON PIXELS to EV: A*x+B : A = 0.0419 , B = 823.0018
    
 
    
import numpy as np
from matplotlib import pyplot as plt


NAMES = ["19", "20", "21", "22"]

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    Nv[name] = SCAN[0]
    Ev[name] = SCAN[0]
    
###NEON Peak

plt.plot(Nv["19"], label = "N 19", color='red')
plt.plot(Nv["20"], label = "N 20", color='blue')
plt.plot(Nv["21"], label = "N 21", color='green')
plt.plot(Nv["22"], label = "N 22", color='purple')
plt.plot(Ev["19"], label = "E 19", color='red', linestyle='--')
plt.plot(Ev["20"], label = "E 20", color='blue', linestyle='--')
plt.plot(Ev["21"], label = "E 21", color='green', linestyle='--')
plt.plot(Ev["22"], label = "E 22", color='purple', linestyle='--')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon and Electron Scan Values')
plt.xlabel('Number')
plt.ylabel('eV')

# Display the plot
plt.show()


NAMES = ["19", "20", "21", "22"]

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]
    EV[name] = []; Ev[name] = SCAN[0]
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        mE = np.mean(rE, axis=0)

        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        aE = np.argmax(mE)
        
        print(k,aN, aE)

        plt.plot(mN)
        plt.axvline(x=aN, color='r', linestyle='--')
        plt.axvline(x=a-k*12, color='g', linestyle='--')
        plt.axvline(x=b-k*12, color='g', linestyle='--')
        plt.show()
        #plt.plot(mE)
        #plt.axvline(x=aE, color='r', linestyle='--')
        #plt.show()
        
        NV[name].append(aN)
        EV[name].append(aE)

    break



###NEON
plt.figure(figsize=(8,8))

plt.plot(NV["19"], Nv["19"], label = "19", color='red')
plt.plot(NV["20"], Nv["20"], label = "20", color='blue')
plt.plot(NV["21"], Nv["21"], label = "21", color='green')
plt.plot(NV["22"], Nv["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###ELECTRON
plt.figure(figsize=(8,8))

plt.plot(EV["19"], Ev["19"], label = "19", color='red')
plt.plot(EV["20"], Ev["20"], label = "20", color='blue')
plt.plot(EV["21"], Ev["21"], label = "21", color='green')
plt.plot(EV["22"], Ev["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Electron Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()



###NEON REVERSED
plt.figure(figsize=(8,8))

plt.plot(1840-np.asarray(NV["19"]), Nv["19"], label = "19", color='red')
plt.plot(1840-np.asarray(NV["20"]), Nv["20"], label = "20", color='blue')
plt.plot(1840-np.asarray(NV["21"]), Nv["21"], label = "21", color='green')
plt.plot(1840-np.asarray(NV["22"]), Nv["22"], label = "22", color='purple')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()
        
        
    
NAMES = ["19", "20", "21", "22"]

Min0a,Min0b = 710,730
Min1a,Min1b = 740,760
Min2a,Min2b = 950,1050

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

RESULTS = {}

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]

    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        #E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        #rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        #mE = np.mean(rE, axis=0)
        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        #
        NV[name].append(aN)


###NEON
plt.figure(figsize=(8,8))

plt.plot(NV["19"], Nv["19"], label = "19", color='cornflowerblue', marker="o", linestyle = 'None' )
plt.plot(NV["20"], Nv["20"], label = "20", color='royalblue', marker="o", linestyle = 'None' )
plt.plot(NV["21"], Nv["21"], label = "21", color='blue', marker="o", linestyle = 'None' )
plt.plot(NV["22"], Nv["22"], label = "22", color='mediumblue', marker="o", linestyle = 'None' )

plt.plot((734, 750, 1004), (869, 867.3, 848.7), color="orange", marker="X", linestyle = 'None', label="PHYSICS")
plt.plot(np.arange(500,1200), -0.0743*np.arange(500,1200) + 923.3076, color="green", marker="None", label="FIT", linestyle = 'solid')

# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()


        

NAMES = ["19", "20", "21", "22"]

Min0a,Min0b = 710,730
Min1a,Min1b = 740,760
Min2a,Min2b = 950,1050

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

RESULTS = {}

NV = {}; Nv = {}
EV = {}; Ev = {}

for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []; Nv[name] = SCAN[0]
    RESULTS[f"SCAN{name}"] = []
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        #E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        #rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        #mE = np.mean(rE, axis=0)
        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        #
        NV[name].append(aN)
        RESULTS[f"SCAN{name}"].append(-0.0743*aN + 923.3076)

np.save("SCANCALL.npy", RESULTS, allow_pickle=True)
###NEON
plt.figure(figsize=(8,8))

plt.plot(NV["19"], RESULTS["SCAN19"], label = "19", color='cornflowerblue', marker="o", linestyle = 'None' )
plt.plot(NV["20"], RESULTS["SCAN20"], label = "20", color='royalblue', marker="o", linestyle = 'None' )
plt.plot(NV["21"], RESULTS["SCAN21"], label = "21", color='blue', marker="o", linestyle = 'None' )
plt.plot(NV["22"], RESULTS["SCAN22"], label = "22", color='mediumblue', marker="o", linestyle = 'None' )

plt.plot((734, 750, 1004), (869, 867.3, 848.7), color="orange", marker="X", linestyle = 'None', label="PHYSICS")
plt.plot(np.arange(500,1200), -0.0743*np.arange(500,1200) + 923.3076, color="green", marker="None", label="FIT", linestyle = 'solid')


# Add legend
plt.legend()

# Set title and labels
plt.title('Neon Pixels to eV Callibration')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()

  


NAMES = ["19", "20", "21", "22"]

Min0a,Min0b = 710,730
Min1a,Min1b = 740,760
Min2a,Min2b = 950,1050

Np = (400,0,560,1840)
Ep = (0,500,1696,600)

MAXavgN = 432
MAXavgE = 41
MINavgN = -6
MINavgE = -4.3

RESULTS = {}

NV = {}
EV = {}


for name in NAMES:
    SCAN = np.load(f"/Users/mst/Desktop/ETHCSE/BCA/Data/SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]

    a,b = 800,1200

    NV[name] = []
    EV[name] = []
    
    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        N = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255)
        E = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255)
        #slice roi
        rN = N[ Np[1]:Np[3], Np[0]:Np[2] ]
        rE = E[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #project roi
        mN = np.mean(rN, axis=1)
        mE = np.mean(rE, axis=0)

        aN = a-12*k + np.argmax(mN[a-12*k:b-12*k])
        aE = np.argmax(mE)
        #
        NV[name].append(aN)
        EV[name].append(aE)

Q = 25
###ELECTRON
plt.figure(figsize=(8,8))

plt.plot(EV["19"], -0.0743*np.asarray(NV["19"]) + 923.3076, label = "19", color='cornflowerblue', marker="o", linestyle = 'None' )
plt.plot(EV["20"], -0.0743*np.asarray(NV["20"]) + 923.3076 , label = "20", color='royalblue', marker="o", linestyle = 'None' )
plt.plot(EV["21"], -0.0743*np.asarray(NV["21"]) + 923.3076 , label = "21", color='blue', marker="o", linestyle = 'None' )
plt.plot(EV["22"], -0.0743*np.asarray(NV["22"]) + 923.3076 , label = "22", color='mediumblue', marker="o", linestyle = 'None' )

A,B = np.polyfit(np.asarray(EV["19"][:Q]+EV["20"][:Q]+EV["21"][:Q]+EV["22"][:Q]) , -0.0739*np.asarray(NV["19"][:Q]+NV["20"][:Q]+NV["21"][:Q]+NV["22"][:Q]) + 922.7317, 1)

plt.plot(np.arange(400,1400), A*np.arange(400,1400) + B, color="green", marker="None", label="FIT", linestyle = 'solid')

print(A,B)
# Add legend
plt.legend()

# Set title and labels
plt.title(f'ELECTRON Pixels to eV Callibration {A:.4f} x + {B:.4f}')
plt.xlabel('Pixels')
plt.ylabel('eV')

# Display the plot
plt.show()

        

    
#BACHELOR THESIS MORITZ TANNER 2023
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
import os
import json
import warnings
import time
warnings.simplefilter('ignore')

DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/AVG/DATA.npy", allow_pickle=True).item()

REF = ((734,869), (750,867.3), (1004, 848.7))

N1,N2,N3,N4 = (400,0,560,1840)
E1,E2,E3,E4 = (0,500,1696,600)


A,B = np.polyfit([i[1] for i in REF],[i[0] for i in REF],1)

FF, XX = plt.subplots(1, 1, sharey=True, figsize=(11.69,8.27))

for RUN in ["19", "20", "21", "22"]:
    a,b = 800,1200
    F, X = plt.subplots(1, 2, sharey=True, figsize=(11.69,8.27))
    Vals = []
    Targets = []
    Peaks = []
    PEAKS = []
    L = len(DATA[RUN])
    for I,(VAL,(NP,EP)) in enumerate(DATA[RUN]):
        Vals.append(f"{VAL:.4f}")
        Targets.append(VAL)
        NB, EB = DATA["BKG"]
        #plt.imshow(NB); plt.show()
        #plt.imshow(EB); plt.show()
        NR = (NP-NB)[N2:N4,N1:N3]
        ER = (EP-EB)[E2:E4,E1:E3]
        #plt.imshow(NR); plt.show()
        #plt.imshow(ER); plt.show()
        NP = np.mean(NR, axis=1)
        EP = np.mean(EP, axis=0)
        #plt.plot(NP); plt.show()
        #plt.plot(EP); plt.show()
        Nmax = np.amax(NP); Nmin = np.amin(NP)
        Emax = np.amax(EP); Emin = np.amin(EP)
        Npeak = a-12*I + np.argmax(NP[a-12*I:b-12*I])
        Epeak = np.argmax(EP)

                
        X[0].plot((NP-Nmin)/(Nmax-Nmin) + I, "black");
        X[0].plot(a-I*12, I+(NP[a-I*12]-Nmin)/(Nmax-Nmin), "g.");
        X[0].plot(b-I*12, I+(NP[b-I*12]-Nmin)/(Nmax-Nmin), "g.");
        X[0].plot(Npeak, I+(NP[Npeak]-Nmin)/(Nmax-Nmin), "r."); 
        
        X[1].plot((EP-Emin)/(Emax-Emin) + I, "black");
        X[1].plot(Epeak, I+(EP[Epeak]-Emin)/(Emax-Emin), "r.");

        Peaks.append(Npeak)
        PEAKS.append(Epeak)

    X[0].set_yticks(list(range(L)), Vals)
    X[0].set_ylabel("Target Energy")
    X[0].set_xlabel("Pixel hight Neon Image")
    X[1].set_xlabel("Pixel hight Electron Image")

    X[0].set_title("Neon Projections")
    X[1].set_title("Electron Projections")
    F.suptitle(f'{RUN} PROJECTION PEAKS', fontsize=16)
        
    F.savefig(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/PROJ{RUN}.png")

    XX.plot(Targets,Peaks)
print(A,B)
FF.suptitle("Pixel to EV conversion (Target vs Effective)")
XX.set_title(f"Linear Fit: {A}*X + {B}")
XX.set_xlabel("EV")
XX.set_ylabel("PIXEL HEIGHT")
XX.scatter([i[1] for i in REF], [i[0] for i in REF], color = "black")
XX.plot(np.arange(850,900), A* np.arange(850,900)+B, color="black")

FF.savefig(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/PROJPLOT.png")
    

FFF, XXX = plt.subplots(1, 1, sharey=True, figsize=(11.69,8.27))
XXX.scatter(Peaks[:-10], PEAKS[:-10], color = "blue")
AP, BP = np.polyfit(Peaks[:-10], PEAKS[:-10], 1)
XXX.plot(Peaks[:-10], AP*np.asarray(Peaks[:-10])+BP)
XXX.set_xlabel("Position of Neon Peak")
XXX.set_ylabel("Position of Electron Peak")
XXX.set_title(f"Linear Fit: {AP} * X + {BP}")
FFF.suptitle("Translation of Neon Callibration to Electron Callibration")

FFF.savefig(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/NECONV.png")





    
        

        
    
        
