import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw, ImageFont

Scan19 = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
#Scan20 = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
#Scan21 = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
#Scan22 = np.load("SCAN22.npy", allow_pickle = True).item()["022."]

#print(Scan19[0]) ; print(Scan20[0]) ; print(Scan21[0]) ; print(Scan22[0])

#print(type(Scan19[1][0]["avgN"]))

"""
for val, pics in zip(Scan19[0], Scan19[1]):
    print(val)
    plt.imshow(pics["avgN"])
    plt.show()
"""

#print(f"Scan19: {len(Scan19[1])}")
#print(f"Scan20: {len(Scan20[1])}")
#print(f"Scan21: {len(Scan21[1])}")
#print(f"Scan22: {len(Scan22[1])}")
print(Scan19[1][5]["avgE"].shape)
print(Scan19[1][5]["avgN"].shape)

print(np.amin(Scan19[1][5]["avgE"]))
image = Image.fromarray( ((Scan19[1][5]["avgE"]+6)/47*255 ).astype(np.uint8), mode="L"    )
image.show()
#plt.imshow(Scan19[1][5]["avgE"]/41*255); plt.show()

draw = ImageDraw.Draw(image)
draw.rectangle([(0, 500), (1696, 600)], outline="green", width=4)
image.show()

image = Image.fromarray( ((Scan19[1][5]["avgE"])/41*255 ).astype(np.uint8), mode="L"    )
image.show()

Scan19 = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
Scan20 = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
Scan21 = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
Scan22 = np.load("SCAN22.npy", allow_pickle = True).item()["022."]

#print(Scan19[0]) ; print(Scan20[0]) ; print(Scan21[0]) ; print(Scan22[0])

#print(type(Scan19[1][0]["avgN"]))
print("ID, MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE")


MAXavgN, MAXavgE, MAXmaxN, MAXmaxE = 0,0,0,0
MINavgN, MINavgE, MINmaxN, MINmaxE = 0,0,0,0
for val, pics in zip(Scan19[0], Scan19[1]):
    MAXavgN = max(MAXavgN , np.amax(pics["avgN"]))
    MAXavgE = max(MAXavgE , np.amax(pics["avgE"]))
    MAXmaxN = max(MAXmaxN , np.amax(pics["maxN"]))
    MAXmaxE = max(MAXmaxE , np.amax(pics["maxE"]))
    MINavgN = min(MINavgN , np.amin(pics["avgN"]))
    MINavgE = min(MINavgE , np.amin(pics["avgE"]))
    MINmaxN = min(MINmaxN , np.amin(pics["maxN"]))
    MINmaxE = min(MINmaxE , np.amin(pics["maxE"]))
print("19", MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE)


MAXavgN, MAXavgE, MAXmaxN, MAXmaxE = 0,0,0,0
MINavgN, MINavgE, MINmaxN, MINmaxE = 0,0,0,0
for val, pics in zip(Scan20[0], Scan20[1]):
    MAXavgN = max(MAXavgN , np.amax(pics["avgN"]))
    MAXavgE = max(MAXavgE , np.amax(pics["avgE"]))
    MAXmaxN = max(MAXmaxN , np.amax(pics["maxN"]))
    MAXmaxE = max(MAXmaxE , np.amax(pics["maxE"]))
    MINavgN = min(MINavgN , np.amin(pics["avgN"]))
    MINavgE = min(MINavgE , np.amin(pics["avgE"]))
    MINmaxN = min(MINmaxN , np.amin(pics["maxN"]))
    MINmaxE = min(MINmaxE , np.amin(pics["maxE"]))
print("20", MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE)

MAXavgN, MAXavgE, MAXmaxN, MAXmaxE = 0,0,0,0
MINavgN, MINavgE, MINmaxN, MINmaxE = 0,0,0,0
for val, pics in zip(Scan21[0], Scan21[1]):
    MAXavgN = max(MAXavgN , np.amax(pics["avgN"]))
    MAXavgE = max(MAXavgE , np.amax(pics["avgE"]))
    MAXmaxN = max(MAXmaxN , np.amax(pics["maxN"]))
    MAXmaxE = max(MAXmaxE , np.amax(pics["maxE"]))
    MINavgN = min(MINavgN , np.amin(pics["avgN"]))
    MINavgE = min(MINavgE , np.amin(pics["avgE"]))
    MINmaxN = min(MINmaxN , np.amin(pics["maxN"]))
    MINmaxE = min(MINmaxE , np.amin(pics["maxE"]))
print("21", MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE)

MAXavgN, MAXavgE, MAXmaxN, MAXmaxE = 0,0,0,0
MINavgN, MINavgE, MINmaxN, MINmaxE = 0,0,0,0
for val, pics in zip(Scan22[0], Scan22[1]):
    MAXavgN = max(MAXavgN , np.amax(pics["avgN"]))
    MAXavgE = max(MAXavgE , np.amax(pics["avgE"]))
    MAXmaxN = max(MAXmaxN , np.amax(pics["maxN"]))
    MAXmaxE = max(MAXmaxE , np.amax(pics["maxE"]))
    MINavgN = min(MINavgN , np.amin(pics["avgN"]))
    MINavgE = min(MINavgE , np.amin(pics["avgE"]))
    MINmaxN = min(MINmaxN , np.amin(pics["maxN"]))
    MINmaxE = min(MINmaxE , np.amin(pics["maxE"]))
print("22", MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE)

"""


ID, MAXavgN, MAXavgE, MAXmaxN, MAXmaxE, MINavgN, MINavgE, MINmaxN, MINmaxE
19 420.6060450922812 24.58186915825597 1924.7698189134808 8828.225150905433 -4.5303697031769445 -4.274275200316225 0 0
20 417.5289650187701 26.23041050038549 5061.82615694165 8641.486720321931 -4.991817684067684 -4.290459327777839 0 0
21 376.791656050433 40.23354764466015 1724.6927565392355 10504.554929577465 -5.965431157409725 -3.922734864004312 -0.03319919517102221 0
22 431.12560292709435 37.70610413895804 3720.569416498994 9228.343259557345 -4.161100045879863 -4.238851093858358 0 0







"""

NAMES = ["19", "20", "21", "22"]

MAXavgN = 432
MAXavgE = 41
MAXmaxN = 5062
MAXmaxE = 10504
MINavgN = -6
MINavgE = -4.3
MINmaxN = -0.04
MINmaxE = 0

font = ImageFont.truetype("Andale Mono.ttf", size=20)

Np = (400,0,560,1840)
Ep = (0,500,1696,600)
Nq = (1840, 1000)
Eq = (1100, 1696)
Nxp = Np[2]-Np[0]
Nyp = Np[3]-Np[1]
Exp = Ep[2]-Ep[0]
Eyp = Ep[3]-Ep[1]



SNw, SNl = Nxp*36, Nyp
SEw, SEl = Exp, Eyp*36
FNw, FNl = Nq[1]*6, Nq[0]*6
FEw, FEl = Eq[1]*6, Eq[0]*6


out_SN = Image.new("L", (SNw, SNl), color="white")
out_SE = Image.new("L", (SEw, SEl), color="white")
out_FN = Image.new("L", (FNw, FNl), color="white")
out_FE = Image.new("L", (FEw, FEl), color="white")

draw_SN = ImageDraw.Draw(out_SN)
draw_SE = ImageDraw.Draw(out_SE)
draw_FN = ImageDraw.Draw(out_FN)
draw_FE = ImageDraw.Draw(out_FE)



for name in NAMES:
    SCAN = np.load(f"SCAN{name}.npy", allow_pickle = True).item()[f"0{name}."]
    vals = SCAN[0]

    #loop through AVG
    for k in range(36):
        #load and scale and transform to bytes
        avgN = ((SCAN[1][k]["avgN"]-MINavgN)/(MAXavgN-MINavgN)*255).astype(np.uint8)
        avgE = ((SCAN[1][k]["avgE"]-MINavgE)/(MAXavgE-MINavgE)*255).astype(np.uint8)
        #slice roi
        RavgN = avgN[ Np[1]:Np[3], Np[0]:Np[2] ]
        RavgE = avgE[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #create images
        avgNi =  Image.fromarray(avgN, mode="L")
        avgEi =  Image.fromarray(avgE, mode="L")
        RavgNi = Image.fromarray(RavgN, mode="L")
        RavgEi = Image.fromarray(RavgE, mode="L")
        #paste images
        i,j = k%6, k//6
        out_FN.paste( avgNi ,  (i*Nq[1],j*Nq[0]) )
        out_FE.paste( avgEi ,  (i*Eq[1],j*Eq[0]) )
        out_SN.paste( RavgNi , (k*Nxp, 0) )
        out_SE.paste( RavgEi , (0, k*Eyp) )
        #write Ev's and textpositions
        text = f"{vals[k]:.2f} eV"
        text_color = 255 #White
        text_FN = (i * Nq[1]+50, (j+1)*Nq[0]-200)
        text_FE = (i * Eq[1]+50, (j+1)*Eq[0]-200)
        text_SN = (k*Nxp+20,Np[3]-50 )
        text_SE = (20, k*Eyp+30)
        #insert text
        draw_FN.text(text_FN, text, font=font, fill=text_color)
        draw_FE.text(text_FE, text, font=font, fill=text_color)
        draw_SN.text(text_SN, text, font=font, fill=text_color)
        draw_SE.text(text_SE, text, font=font, fill=text_color)

    #Save output
    out_FN.save(f"avgFN{name}.png")
    out_FE.save(f"avgFE{name}.png")
    out_SN.save(f"avgSN{name}.png")
    out_SE.save(f"avgSE{name}.png")

    #progress
    print(f"AVG {name}")

    #loop through MAX
    for k in range(36):
        #load and scale and transform to bytes
        maxN = ((SCAN[1][k]["maxN"]-MINmaxN)/(MAXmaxN-MINmaxN)*255).astype(np.uint8)
        maxE = ((SCAN[1][k]["maxE"]-MINmaxE)/(MAXmaxE-MINmaxE)*255).astype(np.uint8)
        #slice roi
        RmaxN = maxN[ Np[1]:Np[3], Np[0]:Np[2] ]
        RmaxE = maxE[ Ep[1]:Ep[3], Ep[0]:Ep[2] ]
        #create images
        maxNi =  Image.fromarray(maxN, mode="L")
        maxEi =  Image.fromarray(maxE, mode="L")
        RmaxNi = Image.fromarray(RmaxN, mode="L")
        RmaxEi = Image.fromarray(RmaxE, mode="L")
        #paste images
        i,j = k%6, k//6
        out_FN.paste( maxNi ,  (i*Nq[1],j*Nq[0]) )
        out_FE.paste( maxEi ,  (i*Eq[1],j*Eq[0]) )
        out_SN.paste( RmaxNi , (k*Nxp, 0) )
        out_SE.paste( RmaxEi , (0, k*Eyp) )
        #write Ev's and textpositions
        text = f"{vals[k]:.2f} eV"
        text_color = 255 #White
        text_FN = (i * Nq[1]+50, (j+1)*Nq[0]-200)
        text_FE = (i * Eq[1]+50, (j+1)*Eq[0]-200)
        text_SN = (k*Nxp+20,Np[3]-50 )
        text_SE = (20, k*Eyp+30)
        #insert text
        draw_FN.text(text_FN, text, font=font, fill=text_color)
        draw_FE.text(text_FE, text, font=font, fill=text_color)
        draw_SN.text(text_SN, text, font=font, fill=text_color)
        draw_SE.text(text_SE, text, font=font, fill=text_color)

    #Save output
    out_FN.save(f"maxFN{name}.png")
    out_FE.save(f"maxFE{name}.png")
    out_SN.save(f"maxSN{name}.png")
    out_SE.save(f"maxSE{name}.png")

    #progress
    print(f"MAX {name}")

#progress
print(f"DONE!")




    #
    
# creates the avgN slice for Scan21

#Scan = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
#Scan = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
#Scan = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
Scan = np.load("SCAN22.npy", allow_pickle = True).item()["022."]


# Define the region of interest coordinates (top left and bottom right)
roi_top_left = (10, 10)
roi_bottom_right = (50, 50)
# Define the coordinates of the rectangle
p1,p2,p3,p4 = 400,300,560,1500

# Create a list of NumPy 2D arrays representing grayscale images
images = Scan[1]

# Calculate the size of the output image
roi_width = p3-p1
roi_height = p4-p2
output_width = roi_width * len(images)
output_height = roi_height

# Create a new output image with the desired size
output_image = Image.new("RGB", (output_width, output_height), color="white")
draw = ImageDraw.Draw(output_image)
font = ImageFont.truetype("Andale Mono.ttf", size=20)


# Loop through the images and extract the region of interest from each
for i, (image, val) in enumerate(zip(images, Scan[0])):
    roi = (image["avgN"][p2:p4, p1:p3]/432*255).astype(np.uint8)
    roi_image = Image.fromarray(roi, mode="L").convert("RGB")
    # Paste the extracted ROI onto the output image
    output_image.paste(roi_image, (i * roi_width, 0))

    text = f"{val:.2f} eV"
    text_color = (0, 128, 0)  # Green color
    text_position = (i * roi_width+20, roi_height-50)
    draw.text(text_position, text, font=font, fill=text_color)
    

# Save the output image
output_image.save("SliceScan22.png")


# creates the avgN slice for Scan21

#Scan = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
#Scan = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
#Scan = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
Scan = np.load("SCAN22.npy", allow_pickle = True).item()["022."]




# Create a list of NumPy 2D arrays representing grayscale images
images = Scan[1]
width = images[0]["avgN"].shape[1]
height = images[0]["avgN"].shape[0]
# Calculate the size of the output image

output_width = width * 6
output_height = height * 6

# Create a new output image with the desired size
output_image = Image.new("RGB", (output_width, output_height), color="white")
draw = ImageDraw.Draw(output_image)
font = ImageFont.truetype("Andale Mono.ttf", size=40)


# Loop through the images and extract the region of interest from each
for k, (image, val) in enumerate(zip(images, Scan[0])):
    #coords
    i,j = k%6, k//6
    #image
    out = Image.fromarray((image["avgN"]/432*255).astype(np.uint8), mode="L").convert("RGB")
    # Paste the extracted ROI onto the output image
    output_image.paste(out, (i * width, j*height))

    text = f"{val:.2f} eV"
    text_color = (0, 128, 0)  # Green color
    text_position = (i * width+50, (j+1)*height-200)
    draw.text(text_position, text, font=font, fill=text_color)
    

# Save the output image
output_image.save("FullScan22.png")


# creates the avgN slice for Scan21

#Scan = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
#Scan = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
#Scan = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
Scan = np.load("SCAN22.npy", allow_pickle = True).item()["022."]


# Define the region of interest coordinates (top left and bottom right)
roi_top_left = (10, 10)
roi_bottom_right = (50, 50)
# Define the coordinates of the rectangle
p1,p2,p3,p4 = 400,300,560,1500

# Create a list of NumPy 2D arrays representing grayscale images
images = Scan[1]

# Calculate the size of the output image
roi_width = p3-p1
roi_height = p4-p2
output_width = roi_width * len(images)
output_height = roi_height

# Create a new output image with the desired size
output_image = Image.new("RGB", (output_width, output_height), color="white")
draw = ImageDraw.Draw(output_image)
font = ImageFont.truetype("Andale Mono.ttf", size=20)


# Loop through the images and extract the region of interest from each
for i, (image, val) in enumerate(zip(images, Scan[0])):
    roi = (image["maxN"][p2:p4, p1:p3]/2000*255).astype(np.uint8)
    roi_image = Image.fromarray(roi, mode="L").convert("RGB")
    # Paste the extracted ROI onto the output image
    output_image.paste(roi_image, (i * roi_width, 0))

    text = f"{val:.2f} eV"
    text_color = (0, 128, 0)  # Green color
    text_position = (i * roi_width+20, roi_height-50)
    draw.text(text_position, text, font=font, fill=text_color)
    

# Save the output image
output_image.save("SliceScan22maxN.png")


# creates the avgN slice for Scan21

#Scan = np.load("SCAN19.npy", allow_pickle = True).item()["019."]
#Scan = np.load("SCAN20.npy", allow_pickle = True).item()["020."]
#Scan = np.load("SCAN21.npy", allow_pickle = True).item()["021."]
Scan = np.load("SCAN22.npy", allow_pickle = True).item()["022."]




# Create a list of NumPy 2D arrays representing grayscale images
images = Scan[1]
width = images[0]["maxN"].shape[1]
height = images[0]["maxN"].shape[0]
# Calculate the size of the output image

output_width = width * 6
output_height = height * 6

# Create a new output image with the desired size
output_image = Image.new("RGB", (output_width, output_height), color="white")
draw = ImageDraw.Draw(output_image)
font = ImageFont.truetype("Andale Mono.ttf", size=40)


# Loop through the images and extract the region of interest from each
for k, (image, val) in enumerate(zip(images, Scan[0])):
    #coords
    i,j = k%6, k//6
    #image
    out = Image.fromarray((image["maxN"]/2000*255).astype(np.uint8), mode="L").convert("RGB")
    # Paste the extracted ROI onto the output image
    output_image.paste(out, (i * width, j*height))

    text = f"{val:.2f} eV"
    text_color = (0, 128, 0)  # Green color
    text_position = (i * width+50, (j+1)*height-200)
    draw.text(text_position, text, font=font, fill=text_color)
    

# Save the output image
output_image.save("FullScan22maxN.png")

