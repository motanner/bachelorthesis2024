#BACHELOR THESIS MORITZ TANNER 2023
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw, ImageFont
import os
import json
import warnings
import time
warnings.simplefilter('ignore')

DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/AVG/Dataset.npy", allow_pickle=True).item()

an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083

#Neon Pulse Energy is ((np.argmax(np.mean((Epic-Ebkg)[E2:E4, E1:E3], axis=0))-Bp)/Ap - Bn)/An

N1,N2,N3,N4 = (400,0,560,1840)
E1,E2,E3,E4 = (500,0,600,1696)#(0,500,1696,600)


BN,BE = DATA["BKG"]
for RUN in ["19","20","21","22","90","15"]:
    print(RUN)
    NFarray = np.concatenate( [A[1][0]-BN for A in DATA[RUN]], axis=1)
    NFimage = Image.fromarray( ((NFarray-np.amin(NFarray))/(np.amax(NFarray)-np.amin(NFarray))*255).astype(np.uint8) , mode="L")
    NFdraw = ImageDraw.Draw(NFimage)

    NSarray = np.concatenate( [(A[1][0]-BN)[N2:N4,N1:N3] for A in DATA[RUN]], axis=1)
    NSimage = Image.fromarray( ((NSarray-np.amin(NSarray))/(np.amax(NSarray)-np.amin(NSarray))*255).astype(np.uint8) , mode="L")
    NSdraw = ImageDraw.Draw(NSimage)

    EFarray = np.concatenate( [np.flip((A[1][1]-BE).T, axis=0) for A in DATA[RUN]], axis=1)
    EFimage = Image.fromarray( ((EFarray-np.amin(EFarray))/(np.amax(EFarray)-np.amin(EFarray))*255).astype(np.uint8) , mode="L")
    EFdraw = ImageDraw.Draw(EFimage)

    ESarray = np.concatenate( [ np.flip(((A[1][1]-BE).T)[E2:E4, E1:E3],axis=0) for A in DATA[RUN]], axis=1)
    ESimage = Image.fromarray( ((ESarray-np.amin(ESarray))/(np.amax(ESarray)-np.amin(ESarray))*255).astype(np.uint8) , mode="L")
    ESdraw = ImageDraw.Draw(ESimage)

    
    NH,NL = np.size(BN,0), np.size(BN,1)
    EH,EL = np.size(BE,1), np.size(BE,0)

    nH, nL = N4, N3-N1
    eH, eL = E4, E3-E1

    for idx, (Val, (PN,PE)) in enumerate(DATA[RUN]):
        VAL = f"{Val:.4f}" if type(Val)!=str else Val
        UNIT = "EV" if type(Val)!=str else "mBar"
        ENERGY = ((np.argmax(np.mean((PE-BE)[E1:E3,E2:E4], axis=0))-bp)/ap -bn)/an

        NFdraw.line((NL*idx, 0, NL*idx, NH),fill=255,width=4)
        NFdraw.line((NL*idx, 0, NL*idx+NL, 0),fill=255,width=4)
        NFdraw.line((NL*idx+NL, 0, NL*idx+NL, NH),fill=255,width=4)
        NFdraw.line((NL*idx, NH, NL*idx+NL, NH),fill=255,width=4)
        NFdraw.text((0+NL*idx+300, 20), f"N-{RUN}-{idx+1} - C:{ENERGY:.4f} EV - T:{VAL} {UNIT}" , fill=255)

        NSdraw.line((nL*idx, 0, nL*idx, nH),fill=255,width=4)
        NSdraw.line((nL*idx, 0, nL*idx+nL, 0),fill=255,width=4)
        NSdraw.line((nL*idx+nL, 0, nL*idx+nL, nH),fill=255,width=4)
        NSdraw.line((nL*idx, nH, nL*idx+nL, nH),fill=255,width=4)
        NSdraw.text((0+nL*idx+10, 20), f"N-{RUN}-{idx+1} \nC:{ENERGY:.4f} EV \nT:{VAL} {UNIT}" , fill=255)

        EFdraw.line((EL*idx, 0, EL*idx, EH),fill=255,width=4)
        EFdraw.line((EL*idx, 0, EL*idx+EL, 0),fill=255,width=4)
        EFdraw.line((EL*idx+EL, 0, EL*idx+EL, EH),fill=255,width=4)
        EFdraw.line((EL*idx, EH, EL*idx+EL, EH),fill=255,width=4)
        EFdraw.text((0+EL*idx+300, 20), f"E-{RUN}-{idx+1} - C:{ENERGY:.4f} EV - T:{VAL} {UNIT}" , fill=255)

        ESdraw.line((eL*idx, 0, eL*idx, eH),fill=255,width=4)
        ESdraw.line((eL*idx, 0, eL*idx+eL, 0),fill=255,width=4)
        ESdraw.line((eL*idx+eL, 0, eL*idx+eL, eH),fill=255,width=4)
        ESdraw.line((eL*idx, eH, eL*idx+eL, eH),fill=255,width=4)
        ESdraw.text((0+eL*idx+10, 20), f"E-{RUN}-{idx+1} \nC:{ENERGY:.4f} EV \nT:{VAL} {UNIT}" , fill=255)

        for I in range(20,NH-100,100):
            NFdraw.line((0+NL*idx,NH-I,0+NL*idx+40,NH-I),fill=255,width=4)
            NFdraw.text((0+NL*idx+10,NH-I-14), f"{((NH-I-bn)/an):.2f}", fill=255)
            NSdraw.line((0+nL*idx,nH-I,0+nL*idx+10,nH-I),fill=255,width=4)
            NSdraw.text((0+nL*idx+4,nH-I-14), f"{((NH-I-bn)/an):.2f}", fill=255)


        for I in range(40,EH-100,100):
            EFdraw.line((0+idx*EL,EH-I,0+idx*EL+40,EH-I),fill=255,width=4)
            EFdraw.text((0+idx*EL+10,EH-I-14), f"{((((I-bp)/ap)-bn)/an):.2f}", fill=255)
            ESdraw.line((0+idx*eL,eH-I,0+idx*eL+10,eH-I),fill=255,width=4)
            ESdraw.text((0+idx*eL+4,eH-I-14), f"{((((I-bp)/ap)-bn)/an):.2f}", fill=255)
    
    NFimage.save(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/NF{RUN}.png")
    NSimage.save(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/NS{RUN}.png")
    EFimage.save(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/EF{RUN}.png")
    ESimage.save(f"/Users/mst/Desktop/ETHCSE/BCA/AVG/ES{RUN}.png")
    





    
