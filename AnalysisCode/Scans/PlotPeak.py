import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
from PIL import Image, ImageDraw, ImageFont
import os
import json
import warnings
import time
warnings.simplefilter('ignore')

DATA = np.load("/Users/mst/Desktop/ETHCSE/BCA/AVG/Dataset.npy", allow_pickle=True).item()

an,bn = -13.44731516282484,  12416.436565300737
ap,bp = -1.6740953652104333, 2293.3604223182083

#Neon Pulse Energy is ((np.argmax(np.mean((Epic-Ebkg)[E2:E4, E1:E3], axis=0))-Bp)/Ap - Bn)/An

N1,N2,N3,N4 = (400,0,560,1840)
E1,E2,E3,E4 = (500,0,600,1696)#(0,500,1696,600)
L1,L2,L3,L4 = (420,980,550,1030)

BN,BE = DATA["BKG"]


fig = plt.figure(figsize=(8,8))
ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)



ax1.plot([ ((np.argmax(np.mean((P[1][1]-BE)[E1:E3,E2:E4], axis=0))-bp)/ap -bn)/an for P in DATA["19"][15:] ],
            [ np.amax((P[1][0]-BN)[L2:L4,L1:L3]) for P in DATA["19"][15:]])
ax2.plot([ ((np.argmax(np.mean((P[1][1]-BE)[E1:E3,E2:E4], axis=0))-bp)/ap -bn)/an for P in DATA["20"][15:] ],
            [ np.amax((P[1][0]-BN)[L2:L4,L1:L3]) for P in DATA["20"][15:]])
ax3.plot([ ((np.argmax(np.mean((P[1][1]-BE)[E1:E3,E2:E4], axis=0))-bp)/ap -bn)/an for P in DATA["21"][15:] ],
            [ np.amax((P[1][0]-BN)[L2:L4,L1:L3]) for P in DATA["21"][15:]])
ax4.plot([ ((np.argmax(np.mean((P[1][1]-BE)[E1:E3,E2:E4], axis=0))-bp)/ap -bn)/an for P in DATA["22"][15:] ],
            [ np.amax((P[1][0]-BN)[L2:L4,L1:L3]) for P in DATA["22"][15:]])
plt.show()
